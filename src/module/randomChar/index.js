//生成一个6位数的随机验证码
const verification = "qwertyuioplkjhgfdsazxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM0123456789";
module.exports = function () {
    let res = "";
    for (var i = 0; i < 8; i++) {
        //求0-verification.length-1  中随机整数
        var random = Math.random();//取0-1 随机小数 包含0  不包含1
        random = random * (verification.length); //0-verification.length  随机数
        random = Math.floor(random);//向下取整  0-verification.length-1  
        res += verification[random]
    }
    return res
}