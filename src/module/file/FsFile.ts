import fs from 'fs'
class SerialFile {
    constructor(debug) {
        this._DEBUG = debug
    }
    fileDirection = './log'
    suffix = '.log'
    _DEBUG = true
    file = this.fileDirection + this.todayDir() + this.todayFile()

    todayDir() {
        return '/' + new Date().toLocaleDateString()
    }
    todayFile() {
        return '/' + new Date().getTime() + this.suffix
    }
    initFile() {
        if (!fs.existsSync(this.fileDirection))
            fs.mkdirSync(this.fileDirection)
        if (!fs.existsSync(this.fileDirection + this.todayDir()))
            fs.mkdirSync(this.fileDirection + this.todayDir())
    }
    appendWithConsole(text) {
        if (this._DEBUG)
            console.log(text);
        this.append(text)
    }
    append(text) {
        this.initFile()
        fs.appendFileSync(this.file, `${new Date().toLocaleString()} : ${text} \n`)
    }
}
module.exports = SerialFile