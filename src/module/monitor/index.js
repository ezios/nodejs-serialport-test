const randomChar = require('../randomChar')
class MonitorModel {
    sendData = ''
    recivedData = ''
    count = 0
    lost = 0
    err = 0
    timer = 0
    config = null
    constructor(config){
        this.config = config
    }

    //根据属性名称++
    plus(name){
        try{
            this[name] ++
        }catch(e){
            console.error(`${name} 不是数字，无法增加！`)
        }
    }
    //生成随机数据
    randomData(){
        this.sendData = randomChar()
        return this.sendData
    }
    //计数并设置计时器时长
    restart(){
        this.recivedData = ''
        this.plus('count')
        this.timer = this.config.interval
    }
    //接收数据
    recive(data){
        this.recivedData += data
    }
    //是否有接收内容
    haveRecived(){
        return this.recivedData.length > 0
    }
    //长度是否相等
    lengthEqual(r = this.recivedData,s = this.sendData){
        return r.length == s.length
    }
    //校验是否一样
    equal(r = this.recivedData,s = this.sendData){
        return (r == s)
    }
    //错误率
    errorRate(){
        return this.rateKeepFourDecimals(this.err,this.count)
    }
    //丢包率
    lostRate(){
        return this.rateKeepFourDecimals(this.lost,this.count)
    }
    //计算率
    rateKeepFourDecimals(numerator , denominator){
        return Math.round(numerator/denominator * 1000000)/10000
    }
}

module.exports = MonitorModel

const m = new MonitorModel()
console.log(
    __dirname
);