# nodejs-serialport-test

## 实现目标
1. 通过串口发送8字节数据
2. 接收串口返回的8字节数据
3. 校验数据是否准确
4. 统计结果并记录错误数据

## 准备工具
1. 虚拟串口调试工具virtual serialport driver 8
原本使用的9.0版本无法使用，后搜寻到8.0版本可以使用，原因未知。
通过该工具能够创建虚拟的串口对，然后在电脑上可以进行调试工作：
![vsd8](https://ezios.gitee.io/2020/10/10/%E4%BD%BF%E7%94%A8nodejs%E7%9A%84serialPort%E5%BA%93%E5%AE%9E%E7%8E%B0com%E5%8F%A3%E8%B0%83%E8%AF%95/vsd8.png)
2. vscode

## 开发环境
1. nodejs v12.12.0
[下载安装](http://nodejs.cn/download/)
2. 依赖
```json
    "readline-sync": "^1.4.10",
    "serialport": "^9.0.1"
```

## 思路
用户录入要使用的串口，通过串口每3秒发送一次8字节的数据，并在3秒间隔中接收返回的数据进行校验，记录校验结果到文件。

## 打包使用
因为该需求是硬件开发同事的需求，所以他没有node环境
google后选用 [pkg](https://github.com/vercel/pkg) 来打包
```shell
npm install -g pkg
pkg -t win index.js #-t是目标环境 
```
环境需要从github下载，比较慢，于是[手动下载](https://github.com/zeit/pkg-fetch/releases)对应的包，然后放到 `用户目录/.pkh-cache/版本/` 下
**记得将名称修改一致**
![修改名称](nodev.png)

配置文件
```javascript
{
    "port"     : "COM2", //串口
    "baudRate" : 115200, //波特率
    "interval" : 3000,   //发送间隔（毫秒）
    "summary"  : 1,      //报告间隔（秒）
    "DEBUG"    : true    //是否开启窗口打印
}
```

### 遇到的坑
pkg打包后 serialport里面bindings包文件依赖没有找到，这种情况目前只能将`exe`文件和`node_module`放一起使用。
